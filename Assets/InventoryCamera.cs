﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCamera : MonoBehaviour
{
    public static InventoryCamera Instance;

    public float Distance = 13.4f;
    public float Duration = 2f;

    public bool Moving;

    private void Awake()
    {
        if(Instance == null) Instance = this;
    }

    public void ToggleInventory(bool active)
    {
        if (!Moving) StartCoroutine(Move(active));
    }
    private IEnumerator Move(bool active)
    {
        Moving = true;
        Debug.Log("Moving");
        Vector3 startPosition = transform.position;
        Vector3 endPosition = startPosition + transform.up * Distance *( active ? 1 : -1) ;
        float t = 0.0f;
        while(t < Duration)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, t/Duration);
            t += Time.unscaledDeltaTime;
            yield return null;
        }

        Moving = false;
    }
}
