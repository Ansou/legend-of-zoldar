﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordPickUp : MonoBehaviour
{

    public GameObject OldMan, Text;
    public float DeathDelay = 0.5f;

    private void Awake()
    {
        if (GameManager.Instance.ExtraRoomData.SwordRoomCleared)
        {
            Destroy(gameObject);
            Destroy(OldMan);
            Destroy(Text);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().HasSword = true;
            Destroy(gameObject);
            Destroy(OldMan, DeathDelay);
            Destroy(Text, DeathDelay);
            FindObjectOfType<SwordIcon>().Enable();
            GameManager.Instance.ExtraRoomData.SwordRoomCleared = true;
        }
    }
}
