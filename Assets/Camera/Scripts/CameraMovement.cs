﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public static CameraMovement Instance;

    [Header("Movement between rooms")]
    [Space] [Header("Camera")]
    public float LerpTime = 1f;
    public float PreFreezeTime = 0.5f, PostFreezeTime = 0.5f;
    [Header("Player")]
    public float PlayerMovementDistance = 1f;
    public float HorizontalStartPercentage = 0.8f;
    public float VerticalStartPercentage = 0.9f;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }


    public IEnumerator MoveToNewRoom(Room newRoom)
    {
        Vector3 targetPosition = newRoom.transform.position; targetPosition.z = transform.position.z;
        Vector3 startPosition = transform.position;
        Vector2 playerMovementDirection = (targetPosition - startPosition).normalized;
        float dot = Mathf.Abs(Vector2.Dot(playerMovementDirection, Vector2.up));
        float percentage = dot > 0.5f ? VerticalStartPercentage : HorizontalStartPercentage;
        bool movingPlayer = false;

        float t = 0.0f;

        GameManager.Instance.Player.Stun(LerpTime + PreFreezeTime + PostFreezeTime, Vector2.zero);

        while (t < PreFreezeTime)
        {
            t += Time.deltaTime;
            yield return null;
        }

        t = 0.0f;
        while (t < LerpTime)
        {
            t += Time.deltaTime;
            if (!movingPlayer && t / LerpTime > percentage){ StartCoroutine(MovePlayer(playerMovementDirection, LerpTime - LerpTime * percentage)); movingPlayer = true; }
            transform.position = Vector3.Lerp(startPosition, targetPosition, t/ LerpTime);
            yield return null;
        }
    }
    private IEnumerator MovePlayer(Vector3 direction, float duration)
    {
        Vector2 playerStartPosition = GameManager.Instance.Player.transform.position;
        Vector2 playerEndPosition = GameManager.Instance.Player.transform.position + direction * PlayerMovementDistance;
        float t = 0f;
        while (t < duration)
        {
            t+= Time.deltaTime;
            GameManager.Instance.Player.transform.position = Vector3.Lerp(playerStartPosition, playerEndPosition, t / duration);
            yield return null;
        }
    }

}
