﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour
{
    public float ShakeMagnitude = 1f;
    public AnimationCurve IntensityToMagnitudeCurve;
    public float MaxDuration = 3f;

    private static float _intensity;

    public float MaxX = 1f, MaxY = 1f, MaxRoll = 1f;
    public float XScale = 5f, YScale = 7f, RollScale = 15f;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha0)) AddIntensity(0.2f);        //TESTING


        _intensity -= Time.deltaTime / MaxDuration;
        _intensity = Mathf.Clamp01(_intensity);

        float magnitude = ShakeMagnitude * IntensityToMagnitudeCurve.Evaluate(_intensity);
        float x = (0.5f - Mathf.PerlinNoise(Time.time * XScale, 0f)) * MaxX * magnitude;
        float y = (0.5f - Mathf.PerlinNoise(0f, Time.time * YScale)) * MaxY * magnitude;
        float roll = (0.5f - Mathf.PerlinNoise(Time.time * RollScale, 0.0f)) * MaxRoll * magnitude;
        transform.localRotation = Quaternion.Euler(0.0f, 0.0f, roll);
        transform.localPosition = new Vector3(x, y, 0.0f);
    }

    public static void AddIntensity(float intensity)
    {
        _intensity += intensity;
        _intensity = Mathf.Clamp01(_intensity);
    }
}
