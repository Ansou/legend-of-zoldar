﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{

    public GameObject[] Drops;

    private void Start ()
	{
	    GetComponent<Health>().OnDeath += DropStuff;
	}
	
    private void DropStuff()
    {
        GameObject item = Drops[Random.Range(0, Drops.Length-1)];
        if (item != null) Instantiate(item, transform.position, Quaternion.identity);
    }
}
