﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementForTesting : MonoBehaviour
{
    public float Speed = 5f;

	private void Update ()
    {
	    transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0) * Speed * Time.deltaTime);	
	}
}
