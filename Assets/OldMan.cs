﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldMan : MonoBehaviour
{

    public int Money;

    private void Awake()
    {
        GameManager.ExtraRoomLoaded.AddListener(ChangeMoney);
    }

    public void ChangeMoney()
    {
        if (Money < 0)
        {
            Inventory.Instance.PayRupies(Money);
        }
        else
        {
            Inventory.Instance.AddRupies(Money);
        }
    }
}
