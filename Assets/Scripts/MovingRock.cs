﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class MovingRock : MonoBehaviour {

	public Vector2 endPos;
	public float speed = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D coll){
		if (coll.gameObject.CompareTag ("Player")) {
			transform.parent.transform.position = Vector2.MoveTowards (transform.parent.transform.position, endPos, speed * Time.deltaTime);
		}
	}
}
