﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using TMPro;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    [Multiline]
    public string Words;

    public float WaitSeconds;

    private string _writtenWords;

    private TextMeshPro _tmp;


    private void Awake()
    {
        _tmp = GetComponent<TextMeshPro>();
        GameManager.ExtraRoomLoaded.AddListener(LoadDialogue);
        _tmp.SetText("");
    }

    public void LoadDialogue()
    {
        StartCoroutine("LoadText");
    }

    private IEnumerator LoadText()
    {
        char[] letters = Words.ToCharArray();
        foreach (var letter in letters)
        {
            _writtenWords += letter;
            _tmp.SetText(_writtenWords);

            yield return new WaitForSeconds(WaitSeconds);
        }
    }

    public void HideText()
    {
        _tmp.SetText("");
    }
}
