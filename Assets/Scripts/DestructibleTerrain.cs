﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DestructibleTerrainType { Bush, Rock }
public class DestructibleTerrain : MonoBehaviour
{
    public DestructibleTerrainType TerrainType;
    public GameObject SecretObject;

    public void Destroy()
    {
        SecretObject.SetActive(true);
        gameObject.SetActive(false);
    }

}
