﻿using System;
using UnityEngine;


[Serializable]
public class Velocity
{
    public float MaxSpeed = 10f;
    public float AccelerationTime = 0.15f;
    public LayerMask CollisionMask;
    public float CollisionMargin = 0.1f;


    private Vector2 velocity_;


    public void AccelerateInDirection(Vector2 input)
    {
        var deltaTime = Time.deltaTime;
        
        var desiredVelocity = input * MaxSpeed;
        
        var accelerationAmount = MaxSpeed / AccelerationTime * deltaTime;

        var desiredAcceleration = desiredVelocity - velocity_;
        if (accelerationAmount > desiredAcceleration.magnitude)
        {
            accelerationAmount = desiredAcceleration.magnitude;
        }
        var accelerationDirection = desiredAcceleration.normalized;
        velocity_ += accelerationDirection * accelerationAmount;
    }


    public Vector2 GetVelocity()
    {
        return velocity_;
    }

    public void SetVelocity(Vector2 velocity)
    {
        velocity_ = velocity;
    }

    public void SetMagnitude(float magnitude)
    {
        velocity_ = velocity_.normalized * magnitude;
    }

    public void FlipX()
    {
        velocity_.x = -velocity_.x;
    }

    public void FlipY()
    {
        velocity_.y = -velocity_.y;
    }

    public void Flip()
    {
        velocity_ = -velocity_;
    }
}