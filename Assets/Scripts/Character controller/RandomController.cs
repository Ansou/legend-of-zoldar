﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class RandomController : Controller2D
{
    public float MinMoveSpeed = 1f;
    public float MaxMoveSpeed = 5f;

    public float MinFindMovementTime = 0.5f;
    public float MaxFindMovementTime = 2f;

    public float DistanceToTileLimit = 0.03f;

    public LayerMask SearchMask;

    protected override void SetDefaultState()
    {
        CurrentState = new RandomStateIdle(this);
        CurrentState.Enter();
    }

    
}