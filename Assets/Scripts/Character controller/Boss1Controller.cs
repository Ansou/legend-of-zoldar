﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Boss1Controller : Controller2D
{
    public float MinMoveTime = 0.5f;
    public float MaxMoveTime = 1f;
    public float MinAttackTime = 0.5f;
    public float MaxAttackTime = 2f;
    public float AttackWaitTime = 0.5f;

	public GameObject projectile;
	public float projectileSpeed = 2f;
	public int projectileDamage = 2;
	public float projectileKnockbackTime = 1f;
	public float projectileKnockbackSpeed = 4f;
	public float spreadAngle = 30f;

	private PlayerController player;

    protected override void Start()
    {
        GetComponent<Health>().OnDamaged += OnDamage;
		player = FindObjectOfType<PlayerController> ();
        base.Start();
    }


    protected override void SetDefaultState()
    {
        CurrentState = new Boss1StateIdle(this);
        CurrentState.Enter();
    }

    public IEnumerator Attack()
    {
        //Open mouth
        yield return new WaitForSeconds(AttackWaitTime);
		Vector3 dir = player.transform.position - transform.position;

		Vector3 dir1 = Quaternion.Euler (0f, 0f, spreadAngle) * dir;
		Vector3 dir2 = Quaternion.Euler (0f, 0f, -spreadAngle) * dir;

		GameObject projectile3 = GameObject.Instantiate(projectile, transform.position, Quaternion.identity);
		projectile3.GetComponent<Projectile>().OnSpawn(new Vector2(dir.x, dir.y).normalized, projectileDamage, projectileSpeed, projectileKnockbackTime, projectileKnockbackSpeed);

		GameObject projectile1 = GameObject.Instantiate(projectile, transform.position, Quaternion.identity);
		projectile1.GetComponent<Projectile>().OnSpawn(new Vector2(dir1.x, dir1.y).normalized, projectileDamage, projectileSpeed, projectileKnockbackTime, projectileKnockbackSpeed);

		GameObject projectile2 = GameObject.Instantiate(projectile, transform.position, Quaternion.identity);
		projectile2.GetComponent<Projectile>().OnSpawn(new Vector2(dir2.x, dir2.y).normalized, projectileDamage, projectileSpeed, projectileKnockbackTime, projectileKnockbackSpeed);
    }


    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            var direction = (coll.transform.position - transform.position).normalized;
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)) direction.y = 0f;
            else direction.x = 0f;
            direction.Normalize();
            coll.gameObject.GetComponent<Health>().Damage(CollisionDamage, CollisionKnockbackTime,
                CollisionKnockbackSpeed * direction);
        }
    }


    private void OnDamage(int damage, float kbTime, Vector2 dir)
    {
        Debug.Log("Boss damaged");
    }
}