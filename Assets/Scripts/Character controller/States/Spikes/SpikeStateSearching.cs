﻿using UnityEngine;

public class SpikeStateSearching : CharacterState
{
    private readonly SpikeController controller_;
    private readonly Vector2[] searchDirections_ = {Vector2.up, Vector2.right, Vector2.down, Vector2.left};


    public SpikeStateSearching(SpikeController controller)
    {
        controller_ = controller;
    }


    public override void Enter()
    {
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        foreach (var direction in searchDirections_)
        {
            if (Search(direction))
            {
                return new SpikeStateRushing(controller_, direction);
            }
        }

        return null;
    }

    public override CharacterState UpdateMovement()
    {
        return null;
    }


    private bool Search(Vector2 direction)
    {
        return Physics2D.BoxCast(controller_.transform.position, new Vector2(1f, 1f), 0f,  direction, Mathf.Infinity, controller_.SearchMask);
    }
}