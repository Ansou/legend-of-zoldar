﻿using UnityEngine;

public class SpikeStateRushing : CharacterState
{
    private Vector2 direction_;
    private SpikeController controller_;


    public SpikeStateRushing(SpikeController controller, Vector2 direction)
    {
        controller_ = controller;
        direction = direction_;
    }


    public override void Enter()
    {
        controller_.Velocity.SetVelocity(direction_ * controller_.RushSpeed);
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        return null;
    }

    public override CharacterState UpdateMovement()
    {
        if(ControllerHelper.Move(controller_, controller_.GetComponent<BoxCollider2D>().size.x / 2 * Mathf.Max(controller_.transform.lossyScale.x, controller_.transform.lossyScale.y)))
        {
            return new SpikeStateReturning(controller_);
        }
        return null;
    }
}