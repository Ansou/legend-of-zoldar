﻿using UnityEngine;

public class SpikeStateReturning : CharacterState
{
    private SpikeController controller_;


    public SpikeStateReturning(SpikeController controller)
    {
        controller_ = controller;
    }


    public override void Enter()
    {
        controller_.Velocity.Flip();
        controller_.Velocity.SetMagnitude(controller_.ReturnSpeed);
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        return null;
    }

    public override CharacterState UpdateMovement()
    {
        if(ControllerHelper.Move(controller_, controller_.GetComponent<BoxCollider2D>().size.x / 2 * Mathf.Max(controller_.transform.lossyScale.x, controller_.transform.lossyScale.y)))
        {
            return new SpikeStateSearching(controller_);
        }
        return null;
    }
}