﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobbStateIdle : CharacterState
{

    private BlobbController controller_;

	private float velocity = 1f;

	private Vector2 point;

	private Timer waitTimer;

    private readonly Vector2[] searchDirections_ = { Vector2.up, Vector2.right, Vector2.down, Vector2.left };

    public BlobbStateIdle(BlobbController controller)
    {
		
        controller_ = controller;

    }


    public override void Enter()
    {
        controller_.transform.position = new Vector2(Mathf.Round(controller_.transform.position.x),
            Mathf.Round(controller_.transform.position.y));
		point = controller_.transform.position;
		waitTimer = new Timer (Random.Range(controller_.MinWaitTime, controller_.MaxWaitTime));
		SetNewPoint();
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        return null;
    }

    public override CharacterState UpdateMovement()
    {
		waitTimer.Update (Time.deltaTime);
		if (waitTimer.IsDone) {
			controller_.transform.position = Vector2.MoveTowards (controller_.transform.position, point, velocity * Time.deltaTime);

			if (controller_.transform.position == new Vector3(point.x, point.y, controller_.transform.position.z)) {
				SetNewPoint ();
				waitTimer = new Timer (Random.Range(controller_.MinWaitTime, controller_.MaxWaitTime));
			}
		}
			

        return null;
    }

    public void SetNewPoint()
    {
		point += FindNewDirection ();

		point = new Vector2(Mathf.Round(point.x), Mathf.Round(point.y));
		velocity = Random.Range (controller_.MinMoveSpeed, controller_.MaxMoveSpeed);
	}

    public Vector2 FindNewDirection()
    {
        List<Vector2> directions = new List<Vector2>();
        foreach (var direction in searchDirections_)
        {
            if (!Search(direction))
            {
                directions.Add(direction);
            }
        }
        if (directions.Count == 0)
        {
            return Vector2.zero;
        }

        int winner = Random.Range(0, directions.Count);


        return directions[winner];
    }

    private bool Search(Vector2 direction)
    {
		
		return Physics2D.Raycast(controller_.transform.position, direction, 1f, controller_.Velocity.CollisionMask);

    }
}