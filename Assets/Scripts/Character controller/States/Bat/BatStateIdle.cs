﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatStateIdle : CharacterState
{

    private BatController controller_;

	private float velocity = 0f;

	private Vector2 direction;

	private Timer moveTimer;

	private Timer changeDirectionTimer;

	private bool accelerate = true;




    public BatStateIdle(BatController controller)
    {
		
        controller_ = controller;

    }


    public override void Enter()
    {
		moveTimer = new Timer(Random.Range(controller_.MinWaitTime, controller_.MaxWaitTime));
		changeDirectionTimer = new Timer(Random.Range(controller_.MinChangeDirectionTime, controller_.MaxChangeDirectionTime));
		direction.x = Random.Range (-1f, 1f);
		direction.y = Random.Range (-1f, 1f);
		direction.Normalize ();
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        return null;
    }

    public override CharacterState UpdateMovement()
    {
		if (moveTimer.Update (Time.deltaTime)) {
			moveTimer = new Timer(Random.Range(controller_.MinWaitTime, controller_.MaxWaitTime));
		}

		if (changeDirectionTimer.Update (Time.deltaTime)) {
			ChangeDirection ();
			changeDirectionTimer.Reset ();
		}
			

		if (accelerate) {	
			velocity += controller_.LerpDerp * Time.deltaTime;
		} else {
			velocity -= controller_.LerpDerp * Time.deltaTime;
		}
		if (moveTimer.PercentDone > 0.5f)
			accelerate = false;
		else
			accelerate = true;

		controller_.Velocity.SetVelocity (direction * velocity);

		if (ControllerHelper.Move(controller_, controller_.GetComponent<BoxCollider2D>().size.x / 2 * Mathf.Max(controller_.transform.lossyScale.x, controller_.transform.lossyScale.y)))
		{
			

			direction *= -1f;
			ChangeDirection ();
		}


		controller_.transform.eulerAngles = new Vector3(controller_.transform.eulerAngles.x, controller_.transform.eulerAngles.y, Mathf.Atan2(-controller_.Velocity.GetVelocity().normalized.x, controller_.Velocity.GetVelocity().normalized.y) * Mathf.Rad2Deg);

        return null;
    }




	private void ChangeDirection(){

		//Ta fram random rotation
		float degrees = Random.Range(-controller_.maxRotation, controller_.maxRotation);



		direction = Quaternion.Euler (new Vector3 (0, 0, degrees)) * new Vector3 (direction.x, direction.y, 0f);

		controller_.Velocity.SetVelocity (direction);
		//Gå dit

		//om kollision med vägg, hitta ny rotation


	}

   
}