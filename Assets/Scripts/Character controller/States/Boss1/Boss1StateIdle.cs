﻿using System.Collections;
using UnityEngine;


public class Boss1StateIdle : CharacterState
{
    private Boss1Controller controller_;
    private Timer moveTimer_;
    private Timer attackTimer_;


    public Boss1StateIdle(Boss1Controller controller)
    {
        controller_ = controller;
    }


    public override void Enter()
    {
        moveTimer_ = new Timer(Random.Range(controller_.MinMoveTime, controller_.MaxMoveTime));
        attackTimer_ = new Timer(Random.Range(controller_.MinAttackTime, controller_.MaxAttackTime));
        controller_.Velocity.SetVelocity(new Vector2(-1f, 0f));
    }


    public override void Exit()
    {
    }


    public override CharacterState UpdateInput()
    {
        if (moveTimer_.Update(Time.deltaTime))
        {
            controller_.Velocity.FlipX();
            moveTimer_ = new Timer(Random.Range(controller_.MinMoveTime, controller_.MaxMoveTime));
        }

        if (attackTimer_.Update(Time.deltaTime))
        {
            controller_.StartCoroutine(controller_.Attack());
            attackTimer_ = new Timer(controller_.AttackWaitTime + Random.Range(controller_.MinAttackTime, controller_.MaxAttackTime));
        }
        return null;
    }


    public override CharacterState UpdateMovement()
    {
        RaycastHit2D hit = ControllerHelper.Move(controller_, controller_.GetComponent<BoxCollider2D>().size.x/ 2 * Mathf.Max(controller_.transform.lossyScale.x, controller_.transform.lossyScale.y));
        if (hit)
        {
            controller_.Velocity.FlipX();
        }
        return null;
    }

    
}