﻿using UnityEngine;

public class CharacterStateStunned : CharacterState
{
    private Timer timer_;
    private Vector2 speed_;
    private PlayerController Controller;

    public CharacterStateStunned(PlayerController controller, float time, Vector2 speed)
    {
        timer_ = new Timer(time);
        speed_ = speed;
        Controller = controller;
    }


    public override void Enter()
    {
        Controller.Velocity.SetVelocity(speed_);
    }


    public override void Exit()
    {
        //If instant stop then change shit
    }


    public override CharacterState UpdateInput()
    {
        var deltaTime = Time.deltaTime;
        if (timer_.Update(deltaTime))
        {

                return new CharacterStateNormal(Controller);
            
        }

        return null;
    }


    public override CharacterState UpdateMovement()
    {
        ControllerHelper.Move(Controller, Controller.GetComponent<CircleCollider2D>().radius * Mathf.Max(Controller.transform.lossyScale.x, Controller.transform.lossyScale.y));

        return null;
    }
}