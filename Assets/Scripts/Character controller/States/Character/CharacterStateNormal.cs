﻿using UnityEngine;

public class CharacterStateNormal : CharacterState
{
    private PlayerController Controller;
    private Animator animator_;

    public CharacterStateNormal(PlayerController controller)
    {
        Controller = controller;
        animator_ = controller.transform.Find("Mesh").GetComponent<Animator>();
    }


    public override void Enter()
    {
        if (animator_)
        {
            animator_.SetFloat("Speed", Controller.Velocity.GetVelocity().magnitude / Controller.Velocity.MaxSpeed);
        }
    }


    public override void Exit()
    {
        if (animator_)
        {
            animator_.SetFloat("Speed", 0f);
        }
    }


    public override CharacterState UpdateInput()
    {
        var input = new Vector2(Controller.RewiredPlayer.GetAxisRaw("Horizontal"),
            Controller.RewiredPlayer.GetAxisRaw("Vertical"));
        if (input.magnitude > 1f) input.Normalize();
        Controller.Velocity.AccelerateInDirection(input);

        if (animator_)
        {
            animator_.SetFloat("Speed", Controller.Velocity.GetVelocity().magnitude / Controller.Velocity.MaxSpeed);
        }

        if (Controller.RewiredPlayer.GetButtonDown("A") && Controller.HasSword)
        {
            if (animator_)
            {
                animator_.SetTrigger("Attack");
            }

            Controller.transform.eulerAngles = new Vector3(Controller.transform.eulerAngles.x,
                Controller.transform.eulerAngles.y, Controller.InputDirection);
            var health = Controller.GetComponent<Health>();

            Controller.GetComponent<Sword>().Attack(Controller.transform.up,
                health.MaxHealth == health.CurrentHealth);
            return new CharacterStateStunned(Controller, Controller.AttackDuration, Vector2.zero);
        }

        return null;
    }


    public override CharacterState UpdateMovement()
    {
        ControllerHelper.Move(Controller,
            Controller.GetComponent<CircleCollider2D>().radius * Mathf.Max(Controller.transform.lossyScale.x,
                Controller.transform.lossyScale.y));

        return null;
    }
}