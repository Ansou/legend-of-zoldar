﻿public abstract class CharacterState
{
    public abstract void Enter();
    public abstract void Exit();
    public abstract CharacterState UpdateInput();
    public abstract CharacterState UpdateMovement();
}