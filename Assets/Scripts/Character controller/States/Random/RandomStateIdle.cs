﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomStateIdle : CharacterState
{
    private RandomController controller_;

    private readonly Vector2[] searchDirections_ = {Vector2.up, Vector2.right, Vector2.down, Vector2.left};

    public RandomStateIdle(RandomController controller)
    {
        controller_ = controller;
    }


    public override void Enter()
    {
        controller_.transform.position = new Vector2(Mathf.Round(controller_.transform.position.x),
            Mathf.Round(controller_.transform.position.y));
        SetNewMovement();
    }

    public override void Exit()
    {
    }

    public override CharacterState UpdateInput()
    {
        return null;
    }

    public override CharacterState UpdateMovement()
    {
        if (Mathf.Abs(controller_.Velocity.GetVelocity().x) > 0f &&
            Mathf.Abs(controller_.transform.position.x - Mathf.Round(controller_.transform.position.x)) <
            controller_.DistanceToTileLimit * Mathf.Abs(controller_.Velocity.GetVelocity().x) ||
            Mathf.Abs(controller_.Velocity.GetVelocity().y) > 0f &&
            Mathf.Abs(controller_.transform.position.y - Mathf.Round(controller_.transform.position.y)) <
            controller_.DistanceToTileLimit * Mathf.Abs(controller_.Velocity.GetVelocity().y))
        {
            controller_.transform.position = new Vector2(
                controller_.Velocity.GetVelocity().y > 0f
                    ? Mathf.Round(controller_.transform.position.x)
                    : controller_.transform.position.x,
                controller_.Velocity.GetVelocity().x > 0f
                    ? Mathf.Round(controller_.transform.position.y)
                    : controller_.transform.position.y);
            SetNewMovement();
        }

        if (ControllerHelper.Move(controller_, controller_.GetComponent<BoxCollider2D>().size.x / 2 * Mathf.Max(controller_.transform.lossyScale.x, controller_.transform.lossyScale.y)))
        {
            SetNewMovement();
        }

        return null;
    }

    public void SetNewMovement()
    {
        controller_.Velocity.SetVelocity(FindNewDirection() * Random.Range(controller_.MinMoveSpeed, controller_.MaxMoveSpeed));
    }

    public Vector2 FindNewDirection()
    {
        List<Vector2> directions = new List<Vector2>();
        foreach (var direction in searchDirections_)
        {
            if (!Search(direction))
            {
                directions.Add(direction);
            }
        }

        if (directions.Count == 0)
        {
            return Vector2.zero;
        }

        int winner = Random.Range(0, directions.Count);


        return directions[winner];
    }

    private bool Search(Vector2 direction)
    {
        return Physics2D.Raycast(controller_.transform.position, direction, 1f, controller_.SearchMask);
    }
}