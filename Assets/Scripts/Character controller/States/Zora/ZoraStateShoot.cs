﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoraStateShoot : CharacterState {

	private ZoraController controller_;

	private Timer exitTimer_;
	private Timer shootTimer;

	private PlayerController player;

	public ZoraStateShoot(ZoraController controller)
	{
		controller_ = controller;
		exitTimer_ = new Timer(controller_.WaitInAirTimer);
		shootTimer = new Timer(controller_.WaitInAirTimer/2f);
		player = GameObject.FindObjectOfType<PlayerController> ();
	}

	public override void Enter()
	{
		Vector2 cont = new Vector2(controller_.transform.position.x, controller_.transform.position.y);
		Vector2 play = new Vector2(player.transform.position.x, player.transform.position.y);
		Vector2 dir = Quaternion.Euler (0f, 0f, 90f) * (play - cont).normalized;
		controller_.transform.up = new Vector3(dir.x, dir.y, 0f);
	}

	public override void Exit()
	{

	}

	public override CharacterState UpdateInput()
	{
		if (exitTimer_.Update(Time.deltaTime))
		{
			return new ZoraStateBlub(controller_);
		}
		if (shootTimer.Update (Time.deltaTime)) {
			var proj = GameObject.Instantiate(controller_.Projectile, new Vector2(controller_.transform.position.x, controller_.transform.position.y), Quaternion.identity);
			proj.GetComponent<Projectile>().OnSpawn((player.transform.position - controller_.transform.position).normalized, controller_.projectileDamage, controller_.projectileSpeed, controller_.knockTime, controller_.knockSpeed);
		}
		return null;
	}

	public override CharacterState UpdateMovement()
	{

		return null;
	}
}
