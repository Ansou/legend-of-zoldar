﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoraStateBlub : CharacterState {

	private ZoraController controller_;

	private Timer exitTimer_;
	private Timer relocateTimer;

	public ZoraStateBlub(ZoraController controller)
	{
		controller_ = controller;
		exitTimer_ = new Timer(controller_.WaitInWaterTimer);
		relocateTimer = new Timer(controller_.WaitInWaterTimer/2f);
	}

	public override void Enter()
	{
		//Play blub
		controller_.model.SetActive(false);
		controller_.splash.Play ();
		controller_.GetComponent<BoxCollider2D>().enabled = false;
	}

	public override void Exit()
	{
		//Stop play blub
		controller_.model.SetActive(true);
		controller_.splash.Play ();
		controller_.GetComponent<BoxCollider2D>().enabled = true;
	}

	public override CharacterState UpdateInput()
	{
		if (exitTimer_.Update(Time.deltaTime))
		{
			return new ZoraStateShoot(controller_);
		}
		return null;
	}

	public override CharacterState UpdateMovement()
	{
		if (relocateTimer.Update (Time.deltaTime)) {
			controller_.transform.localPosition = controller_.ShootPoints [Random.Range (0, controller_.ShootPoints.Length - 1)];
		}

		return null;
	}
}
