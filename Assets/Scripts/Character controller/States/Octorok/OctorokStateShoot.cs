﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctorokStateShoot : CharacterState {

    private OctorokController controller_;

    private Timer exitTimer_;

    public OctorokStateShoot(OctorokController controller)
    {
        controller_ = controller;
        exitTimer_ = new Timer(Random.Range(controller_.MinWaitShootTime, controller_.MaxWaitShootTime));
    }

    public override void Enter()
    {
        controller_.Velocity.SetVelocity(Vector2.zero);
    }

    public override void Exit()
    {
        if (Random.Range(1, 100) < controller_.ChanceToShoot * 100)
        {
            GameObject projectile = GameObject.Instantiate(controller_.projectile, controller_.transform.position, controller_.transform.rotation);
			projectile.GetComponent<Projectile>().OnSpawn(controller_.transform.up, controller_.ProjectileDamage, controller_.ProjectileSpeed, controller_.ProjectileKnockTime, controller_.ProjectileKnockSpeed);
        }
    }

    public override CharacterState UpdateInput()
    {
        if (exitTimer_.Update(Time.deltaTime))
        {
            return new OctorokStateIdle(controller_);
        }
        return null;
    }

    public override CharacterState UpdateMovement()
    {
        return null;
    }
}
