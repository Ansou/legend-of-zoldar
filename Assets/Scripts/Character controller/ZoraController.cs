﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class ZoraController : Controller2D
{
	public float WaitInWaterTimer = 2f;

	public float WaitInAirTimer = 2f;

	public Vector2[] ShootPoints;

	public GameObject Projectile;
	public int projectileDamage = 1;
	public float projectileSpeed = 4f;
	public float knockTime = 1f;
	public float knockSpeed = 5f;

	[HideInInspector]
	public GameObject model;
	[HideInInspector]
	public ParticleSystem splash;

	protected override void SetDefaultState()
	{
		model = transform.GetChild (0).gameObject;
		splash = transform.GetChild (1).GetComponent<ParticleSystem>();
		CurrentState = new ZoraStateBlub(this);
		CurrentState.Enter();
	}


}