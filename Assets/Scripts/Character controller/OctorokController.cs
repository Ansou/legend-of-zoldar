﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class OctorokController : Controller2D
{
    public float MinMoveSpeed = 1f;
    public float MaxMoveSpeed = 5f;

    public float DistanceToTileLimit = 0.03f;

    public float ChanceToShoot = 0.7f;

    public float MinWaitShootTime = 0.2f;
    public float MaxWaitShootTime = 1f;

	public float ProjectileKnockTime = 0.2f;
	public float ProjectileKnockSpeed = 3f;

	public float MinIdleTime = 1f;
	public float MaxIdleTime = 5f;
    
    public GameObject projectile;

    public int ProjectileDamage = 1;
    public float ProjectileSpeed = 3f;

	public float MinTurnTime = 1f;
	public float MaxTurnTime = 3f;

    protected override void SetDefaultState()
    {
        CurrentState = new OctorokStateIdle(this);
        CurrentState.Enter();
    }


}