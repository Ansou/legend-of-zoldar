﻿using Rewired;
using UnityEngine;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(CircleCollider2D))]
public class PlayerController : Controller2D
{
    public static PlayerController Instance;

    public float RotationInputSmoothingTime = 0.1f;
    public Transform Sword;
    public float AttackDuration = 0.3f;


    public Player RewiredPlayer;

    public bool HasSword
    {
        get { return hasSword_; }
        set
        {
            hasSword_ = value;
            Sword.gameObject.SetActive(value);
        }
    }

    private Vector2 smoothedDirectionInput_;
    [SerializeField]private bool hasSword_;


    public float InputDirection { get; private set; }

    private void Awake()
    {
        base.Awake();
        Instance = this;
    }

    protected override void Start()
    {
        RewiredPlayer = ReInput.players.GetPlayer(0);
        var health = GetComponent<Health>();
        health.OnDeath += OnDeath;
        health.OnDamaged += OnDamaged;

        Sword.gameObject.SetActive(HasSword);

        base.Start();
    }


    private new void Update()
    {
        SmoothRotationInput();
        ControllerHelper.RotateToward(this, InputDirection);

        base.Update();
    }



    protected override void SetDefaultState()
    {
        CurrentState = new CharacterStateNormal(this);
        CurrentState.Enter();
    }

    public override void Stun(float time, Vector2 speed)
    {
        SwitchState(new CharacterStateStunned(this, time, speed));
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        //Du gick på något
    }


    private void OnDeath()
    {
        SuperAudioManager.AudioInstance.PlayerAction("PlayerDie");
        GetComponent<Health>().Heal(6);

        GameManager.Instance.ResetOnDeath();
    }


    private void OnDamaged(int damage, float knockbackTime, Vector2 knockbackVelocity)
    {
        Stun(knockbackTime, knockbackVelocity);
    }


    // Input smoothing, should be run every frame regardless of state
    private void SmoothRotationInput()
    {
        var deltaTime = Time.unscaledDeltaTime;

        var input = new Vector2(-RewiredPlayer.GetAxisRaw("Horizontal"), RewiredPlayer.GetAxisRaw("Vertical"));
        if (input.magnitude > 1f) input.Normalize();

        var inputSmoothingMovement = 1f / RotationInputSmoothingTime * deltaTime;
        var inputSmoothingDirection = input - smoothedDirectionInput_;
        if (inputSmoothingMovement > inputSmoothingDirection.magnitude)
            inputSmoothingMovement =
                inputSmoothingDirection.magnitude;
        inputSmoothingDirection.Normalize();
        smoothedDirectionInput_ += inputSmoothingDirection * inputSmoothingMovement;

        if (input.magnitude > 0f)
        {
            InputDirection = Mathf.Rad2Deg * Mathf.Atan2(smoothedDirectionInput_.x, smoothedDirectionInput_.y);
        }
    }
}