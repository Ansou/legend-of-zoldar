﻿using UnityEngine;


public abstract class Controller2D : MonoBehaviour
{
    [SerializeField] private Velocity velocity_;

    public float MaxRotationTime = 0.15f;

    public int CollisionDamage = 1;
    public float CollisionKnockbackTime = 0.3f;
    public float CollisionKnockbackSpeed = 4f;


    public Velocity Velocity
    {
        get { return velocity_; }
    }


    protected CharacterState CurrentState;


    public void Awake()
    {
        SetDefaultState();
    }


    // Use this for initialization
    protected virtual void Start()
    {
		GetComponent<Health> ().OnDeath += OnDeath;
    }

	private void OnDeath(){
        if(this is PlayerController) return;
	    gameObject.SetActive(false);
	}


    // Update is called once per frame
    protected void Update()
    {
        var newState = CurrentState.UpdateInput();
        if (newState != null)
        {
            SwitchState(newState);
        }
    }


    private void FixedUpdate()
    {
        var newState = CurrentState.UpdateMovement();
        if (newState != null)
        {
            SwitchState(newState);
        }
    }


    protected void SwitchState(CharacterState state)
    {
        CurrentState.Exit();
        CurrentState = state;
        CurrentState.Enter();
    }


    protected abstract void SetDefaultState();





    public virtual void Stun(float time, Vector2 speed)
    {
    }

	private void OnEnable(){
		SetDefaultState ();
		GetComponent<Health> ().OnDeath += OnDeath;
	}

    private void OnCollisionEnter2D(Collision2D col)
    {
		if (col.gameObject.CompareTag ("Player")) {
			var health = col.gameObject.GetComponent<Health>();
			if (health)
			{
				var direction = col.transform.position - transform.position;
				if (direction.x < direction.y)
					direction.x = 0f;
				else
					direction.y = 0f;
				health.Damage(CollisionDamage, CollisionKnockbackTime, direction.normalized * CollisionKnockbackSpeed);
			}
		}
	}
}