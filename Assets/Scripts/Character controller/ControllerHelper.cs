﻿using UnityEngine;

public static class ControllerHelper
{
    public static RaycastHit2D Move(Controller2D controller, float colliderRadius)
    {
        var deltaTime = Time.deltaTime;
        
        var movement = controller.Velocity.GetVelocity() * deltaTime;
        var hit = Physics2D.CircleCast(controller.transform.position, colliderRadius,
            controller.Velocity.GetVelocity().normalized,
            movement.magnitude + controller.Velocity.CollisionMargin, controller.Velocity.CollisionMask);
        if (hit)
        {

            Vector2 point = Vector2.MoveTowards(controller.transform.position, hit.centroid,
                Vector2.Distance(controller.transform.position, hit.centroid) -
                controller.Velocity.CollisionMargin);
            if (Vector2.Angle(controller.Velocity.GetVelocity().normalized, Quaternion.Euler(0f, 0f, 90f) * hit.normal) < Vector2.Angle(controller.Velocity.GetVelocity().normalized, Quaternion.Euler(0f, 0f, -90f) * hit.normal))
            {

                var newMovement = new Vector2((Quaternion.Euler(0f, 0f, 90f) * hit.normal).x,
                                      (Quaternion.Euler(0f, 0f, 90f) * hit.normal).y) * Time.deltaTime;
                var newHit = Physics2D.CircleCast(point, colliderRadius,
                    newMovement.normalized,
                    newMovement.magnitude + controller.Velocity.CollisionMargin, controller.Velocity.CollisionMask);
                if (newHit)
                {
                    point = Vector2.MoveTowards(point, newHit.centroid,
                        Vector2.Distance(point, newHit.centroid) -
                        controller.Velocity.CollisionMargin);
                    controller.transform.position = point;
                    return hit;
                }

                point += new Vector2((Quaternion.Euler(0f, 0f, 90f) * hit.normal).x, (Quaternion.Euler(0f, 0f, 90f) * hit.normal).y) * Time.deltaTime;
            }
            else if (Vector2.Angle(controller.Velocity.GetVelocity().normalized, Quaternion.Euler(0f, 0f, 90f) * hit.normal) > Vector2.Angle(controller.Velocity.GetVelocity().normalized, Quaternion.Euler(0f, 0f, -90f) * hit.normal))
            {

                var newMovement = new Vector2((Quaternion.Euler(0f, 0f, -90f) * hit.normal).x, (Quaternion.Euler(0f, 0f, -90f) * hit.normal).y) * Time.deltaTime;

                var newHit = Physics2D.CircleCast(point, colliderRadius,
                    newMovement.normalized,
                    newMovement.magnitude + controller.Velocity.CollisionMargin, controller.Velocity.CollisionMask);
                if (newHit)
                {
                    point = Vector2.MoveTowards(point, newHit.centroid,
                        Vector2.Distance(point, newHit.centroid) -
                        controller.Velocity.CollisionMargin);
                    controller.transform.position = point;
                    return hit;
                }

                point += new Vector2((Quaternion.Euler(0f, 0f, -90f) * hit.normal).x, (Quaternion.Euler(0f, 0f, -90f) * hit.normal).y) * Time.deltaTime;
            }


            controller.transform.position = point;

            return hit;
        }

        controller.transform.position = new Vector3(controller.transform.position.x + movement.x, controller.transform.position.y + movement.y, controller.transform.position.z);

        return hit;
    }


    // Adjust player rotation
    public static void RotateToward(Controller2D controller, float angle)
    {
        var deltaTime = Time.deltaTime;

        var currentRotation = controller.transform.eulerAngles.z;
        var rotationAngle = angle - currentRotation;
        rotationAngle = MathFunctions.NormalizeAngle180(rotationAngle);

        var rotationSpeed = 180f / controller.MaxRotationTime * Mathf.Sign(rotationAngle);
        var rotateAmount = rotationSpeed * deltaTime;
        if (Mathf.Abs(rotateAmount) > Mathf.Abs(rotationAngle))
        {
            rotateAmount = rotationAngle;
        }

        currentRotation += rotateAmount;
        currentRotation = MathFunctions.NormalizeAngle(currentRotation);
        controller.transform.eulerAngles = new Vector3(controller.transform.eulerAngles.x,  controller.transform.eulerAngles.y, currentRotation);
    }
}