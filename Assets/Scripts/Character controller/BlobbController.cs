﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class BlobbController : Controller2D
{
	public float MinMoveSpeed = 1f;
	public float MaxMoveSpeed = 3f;

	public float MinWaitTime = 0.5f;
	public float MaxWaitTime = 1.5f;

	protected override void SetDefaultState()
	{
		CurrentState = new BlobbStateIdle(this);
		CurrentState.Enter();
	}


}