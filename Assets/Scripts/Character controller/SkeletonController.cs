﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SkeletonController : Controller2D
{
	public float MinMoveSpeed = 1f;
	public float MaxMoveSpeed = 5f;

	public float DistanceToTileLimit = 0.03f;

	public float MinTurnTime = 1f;
	public float MaxTurnTime = 3f;

	protected override void SetDefaultState()
	{
		CurrentState = new SkeletonStateIdle(this);
		CurrentState.Enter();
	}


}