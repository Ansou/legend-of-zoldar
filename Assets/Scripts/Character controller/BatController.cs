﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class BatController : Controller2D
{

	public float MoveSpeed = 3f;

	public float MinWaitTime = 4f;
	public float MaxWaitTime = 8f;

	public float LerpDerp = 3f;

	public float maxRotation = 50f;

	public float MinChangeDirectionTime = 0.2f;
	public float MaxChangeDirectionTime = 0.8f;


	//Variabel för maxhastighet



	protected override void SetDefaultState()
	{
		CurrentState = new BatStateIdle(this);
		CurrentState.Enter();
	}


}