﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SpikeController : Controller2D
{
    public LayerMask SearchMask;
    public float RushSpeed = 5f;
    public float ReturnSpeed = 3f;


    protected override void SetDefaultState()
    {
        CurrentState = new SpikeStateSearching(this);
        CurrentState.Enter();
    }
}