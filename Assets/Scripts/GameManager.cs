﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
	public static UnityEvent OnPlayerDeath = new UnityEvent();
    public static UnityEvent ExtraRoomLoaded = new UnityEvent();

    public ExtraRoomData ExtraRoomData;

    [Header("Scenes")]
    public int MinSceneId;
    public int MaxSceneId;

	public PlayerController Player{	get{ return PlayerController.Instance;}}

    [Header("Dungeon")]
    public bool LoadDungeon;
    public int MinDungeonSceneId;
    public int MaxDungeonSceneId;

    //Sceneloading
    [SerializeField] private List<int> _loadedScenes = new List<int>();

    //Extra room
    private Room _roomToReturnTo;
    private Transform _returnPoint;
    private string _extraRoomName;

    private bool _loading;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            StartCoroutine(RunFirstTimeSetUp());
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
           Player.GetComponent<Health>().Damage(1000, 0, Vector2.zero);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Inventory.Instance.AddRupies(1000);
        }
    }

    private IEnumerator RunFirstTimeSetUp()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("Gameplay");
        while (!operation.isDone) yield return null;
        yield return LoadAllScenes(LoadDungeon, LoadDungeon);
        yield return null;
        GameObject.FindObjectOfType<InventoryUIHandler>().SetUp();
        ExtraRoomData.SwordRoomCleared = false;
    }

    public void LoadScenes(bool dungeon, bool dungeonRelated)
    {
        if(!_loading) StartCoroutine(LoadAllScenes(dungeon, dungeonRelated));
    }

    private IEnumerator LoadAllScenes(bool loadDungeon, bool dungeonRelated)
    {
        FadeOutPanel.SetAlpha(1.0f);
        _loading = true;
        Player.gameObject.SetActive(false);
        yield return UnloadLoadedScenes();
        List<AsyncOperation> operations = new List<AsyncOperation>();

        int min = loadDungeon ? MinDungeonSceneId : MinSceneId;
        int max = loadDungeon ? MaxDungeonSceneId : MaxSceneId;

        for(int i = min; i <= max; i++){ operations.Add(SceneManager.LoadSceneAsync(i, LoadSceneMode.Additive)); _loadedScenes.Add(i);}

        while (operations.Any(o => !o.isDone)) yield return null;

        SetUp(dungeonRelated);
        Player.gameObject.SetActive(true);
        _loading = false;
        yield return FadeOutPanel.Fade(0.0f);
    }
    private IEnumerator UnloadLoadedScenes()
    {
        List<AsyncOperation> operations = new List<AsyncOperation>();

        foreach (int loadedScene in _loadedScenes)
        {
            AsyncOperation operation = SceneManager.UnloadSceneAsync(loadedScene);
            if(operation == null) continue;
            operations.Add(operation);
        }

        while (operations.Any(o => !o.isDone)) yield return null;

        _loadedScenes.Clear();
    }
    private void SetUp(bool dungeonRelated)
    {
        RoomManager.SetUpRooms();

        if (dungeonRelated)
        {
            Room room = FindObjectOfType<DungeonEntrance>().GetComponentInParent<Room>();
            RoomManager.SetActiveRoom(room, true);
        }
        else
        {
            RoomManager.SetActiveRoom(RoomManager.DefaultRoom, true);
        }
        Player.transform.position = RoomManager.ActiveRoom.transform.position;
    }

    public void LoadExtraRoom(string id, Transform returnPoint, Room room)
    {
        StartCoroutine(LoadExtraRoomCoroutine(id + "ExtraRoom"));
        _roomToReturnTo = room;
        _returnPoint = returnPoint;
        _extraRoomName = id + "ExtraRoom";
    }
    private IEnumerator LoadExtraRoomCoroutine(string sceneName)
    {
        yield return FadeOutPanel.Fade(1.0f);
        
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        while (!operation.isDone) yield return null;

        ExtraRoom extraRoom = FindObjectOfType<ExtraRoom>();
        Player.transform.position = extraRoom.PlayerSpawnPosition;
		Player.Stun (1.0f, Vector2.zero);
        CameraMovement.Instance.transform.position = extraRoom.CameraPosition;

        yield return FadeOutPanel.Fade(0.0f);
        ExtraRoomLoaded.Invoke();
    }

    public void SetReturnPoint(Transform returnPoint)
    {
        _returnPoint = returnPoint;
    }

    public void ReturnTo()
    {
        StartCoroutine(ReturnToRoomCoroutine());
    }
    private IEnumerator ReturnToRoomCoroutine()
    {
        yield return FadeOutPanel.Fade(1.0f);

        Player.transform.position = _returnPoint.position;
        RoomManager.SetActiveRoom(_roomToReturnTo, true);
        SceneManager.UnloadSceneAsync(_extraRoomName);

        yield return FadeOutPanel.Fade(0.0f);
    }

	public void ResetOnDeath() {

		OnPlayerDeath.Invoke ();
        RoomManager.SetDefaultRoomActive ();
        Player.gameObject.transform.position = (RoomManager.ActiveRoom.transform.position);

        //respawna fiender i rum
    }
	
}
