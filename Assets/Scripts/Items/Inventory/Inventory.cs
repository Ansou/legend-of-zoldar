﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Inventory : MonoBehaviour{

    public static Inventory Instance;

    private int Rupies;
    private int Bombs;
    private int Keys;
    private int TriforcePieces;
    public GameObject SwordIcon;
    private const int maxAmountBombs = 8;
    public delegate void ResourceUpdateEvent(int num);
    //public delegate void CurrencyUpdateEvent(int org, int num);
    public delegate void ItemUpdateEvent(int slotIndex, bool active);
    public delegate void ItemEquipEvent(int slotIndex);

    //public CurrencyUpdateEvent OnRupiesUpdate = delegate { };
    public ResourceUpdateEvent OnBombsUpdate = delegate { };
    public ResourceUpdateEvent OnKeysUpdate = delegate { };
    public ItemUpdateEvent OnItemAdded = delegate { };
    public ItemEquipEvent OnEquip = delegate { };
    


    // TEMPORARY, delete later


    

    //private Dictionary<string, equipable> Equipables;     //data collection for equipables
    public Item[] Items;                             //data collection for items
    private bool[] itemsActive;
    private Item CurrentlySelected;
    //ui reference

    //equipable item dictionary
    //player reference

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogWarning("Multiple inventories!!!");
        }
    }

    public void SetUp()
    {
        foreach(Item i in Items) {
            if(i != null)
                i.Initilize();
        }
        itemsActive = new bool[Items.Length];
        for (var i = 0; i < itemsActive.Length; ++i)
            itemsActive[i] = false;
        for (var i = 0; i < itemsActive.Length; ++i)
        {
            OnItemAdded(i, itemsActive[i]);
        }
        CurrentlySelected = null;
        
    }

    public void Update()
    {
        if (PlayerController.Instance.RewiredPlayer.GetButtonDown("B") && CurrentlySelected != null)
        {
            CurrentlySelected.Use();
        }
    }

    public void AddRupies(int amount)                       
    {
        var original = Rupies;
        if (amount <= 0)
            return;
        Rupies += amount;
        //OnRupiesUpdate(original, Rupies);
    }

    public bool PayRupies(int amount)                      
    {
        if(Rupies - amount < 0)
        {
            return false;
        }
        var original = Rupies;
        Rupies -= amount;
        //OnRupiesUpdate(original, Rupies);
        return true;
       
    }

    public int GetRupies()
    {
        return Rupies;
    }

    public bool FullOnBombs()
    {
        if (Bombs == maxAmountBombs)
            return true;

        return false;
    }

    public bool ConsumeBomb()                               
    {
        if(Bombs >= 1)
        {
            
            Bombs -= 1;
            OnBombsUpdate(Bombs);
            return true;
        }

        return false;
       
    }

    public void AddBomb()
    {
        Bombs++;
        OnBombsUpdate(Bombs);
    }

    public void AddKey()
    {
        Keys++;
        OnKeysUpdate(Keys);
    }

    public bool ConsumeKey()                                
    {
        if(Keys >= 1)
        {
            Keys -= 1;
            OnKeysUpdate(Keys);
            return true;
        }

        return false;
    }

    

    public void SetItemActive<T>(bool active)
    {
        int idOfItem = GetItemIdInList<T>();

        if (idOfItem != -1)
        {
            itemsActive[idOfItem] = active;
            OnItemAdded(idOfItem, active);
        }

        
        //add an item to dictionary
    }


    public Item GetItemInIndex(int index)
    {
        return Items[index];
    }

    public bool HasItem(Item item)
    {
        for(var i = 0; i < Items.Length; ++i)
        {
            if (Items[i] == item)
                return itemsActive[i];
        }
        return false;
        
    }

    public bool HasItem<T>()
    {
        int itemId = GetItemIdInList<T>();
        return itemId != -1 && itemsActive[itemId];
    }

    private int GetItemIdInList<T>()
    {
        for (int i = 0; i < Items.Length; i++)
        {
            if (Items[i] != null && Items[i].GetType() == typeof(T))
            {
                return i;
            }
        }
        return -1;
    }

    public bool ItemIsEquiped<T>()
    {
        if (CurrentlySelected.GetType() == typeof(T))
            return true;

        return false;
    }

    /*public void AddEquipable(Equipable equipable)
    {
        equipables.Add(equipable.ID, equipable);
    }*/

    public void Equip(int index)
    {
        CurrentlySelected = Items[index];
        OnEquip(index);
    }

    public void EquipSword()
    {
        SwordIcon.SetActive(true);
    }

    public void DequipItem()
    {
        CurrentlySelected = null;
        OnEquip(-1);
    }
}
