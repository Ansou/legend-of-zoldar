﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventSystemController : MonoBehaviour {

    private EventSystem eventSystem;
    private bool Paused;

    public delegate void EventSystemChange();

    public EventSystemChange OnPause = delegate { };
    public EventSystemChange OnStart = delegate { };


	// Use this for initialization
	void Start ()
    {
        eventSystem = EventSystem.current;
        eventSystem.sendNavigationEvents = false;
        Paused = false;
	}

    private void Update()
    {
        if (PlayerController.Instance.RewiredPlayer.GetButtonDown("Start") && !InventoryCamera.Instance.Moving)
        {
            if (Paused)
            {
                Paused = false;
                Time.timeScale = 1;
                //move ui canvas
                InventoryCamera.Instance.ToggleInventory(false);
                eventSystem.sendNavigationEvents = false; //after ui has been moved
               
            }

            else
            {
                InventoryCamera.Instance.ToggleInventory(true);
                Paused = true;
                OnPause();
                Time.timeScale = 0;
                //move ui canvas
                eventSystem.sendNavigationEvents = true; //after UI has been moved
            }
        }
    }
    


}
