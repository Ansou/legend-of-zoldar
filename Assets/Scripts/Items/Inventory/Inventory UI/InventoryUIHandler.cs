﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryUIHandler : MonoBehaviour {

    private Button[] Buttons;
    public Text RubiesAmount;
    public Text BombsAmount;
    public Text KeysAmount;
    public Image Image;
    private int current;

    // Use this for initialization

    private float delay = 0.1f;
    private float time;

    public void SetUp ()
    {
        Buttons = GetComponentsInChildren<Button>();
        var inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        inventory.OnItemAdded += SetInteractable;
        //inventory.OnRupiesUpdate += UpdateRupies;
        inventory.OnBombsUpdate += UpdateBombs;
        inventory.OnKeysUpdate += UpdateKeys;
        inventory.OnEquip += SetEquipImage;
        GameObject.Find("Event System Controller").GetComponent<EventSystemController>().OnPause += SetFirstActive;
        current = 0;
        Inventory.Instance.SetUp();
        FindObjectOfType<HealthUI>().SetUp();
        time = Time.time;

    }
	
	// Update is called once per frame

    public void SetInteractable(int slotIndex, bool active)
    {
        Buttons[slotIndex].interactable = active;
        Buttons[slotIndex].image.enabled = active;
    }

    private void SetEquipImage(int index)
    {
        Buttons = GetComponentsInChildren<Button>();
        if (index == -1)
        {
            Image.enabled = false;
            return;
        }
        Image.enabled = true;
        Image.sprite = Buttons[index].GetComponent<Image>().sprite;
        
    }

    public void SetFirstActive()
    {
        for (var i = 0; i < Buttons.Length; ++i)
        {
            if (Buttons[i] != null && Buttons[i].IsInteractable())
            {
                StartCoroutine(DelayButtonSelect(Buttons[i]));
            }
        }
    }

    /*public void UpdateRupies(int original, int target)
    {
        RubiesAmount.text = "" + original;
        StartCoroutine(RubiesChangeOverTime(original, target));
    }*/

    public void UpdateKeys(int num)
    {
        KeysAmount.text = "" + num;
    }

    public void Update()
    {
        time += Time.unscaledDeltaTime;
        if (time < delay) return;
        time = 0.0f;

        int target = Inventory.Instance.GetRupies();
        int change = target > current ? 1 : -1;
        if(current != target)
        {
            current += change;
            RubiesAmount.text = "" + current;
        }
    }

    public void UpdateBombs(int num)
    {
        if (num == 0)
        {
            Buttons[1].interactable = false;
            Buttons[1].image.enabled = false;
            if (Inventory.Instance.ItemIsEquiped<Bomb>())
                Inventory.Instance.DequipItem();
        }
        else if (num == 1)
        {
            Buttons[1].interactable = true;
            Buttons[1].image.enabled = true;

        }

        BombsAmount.text = "" + num;
    }

    /*private IEnumerator RubiesChangeOverTime(int current, int target)
    {
        
        int change = (target - current) > 0 ? 1 : -1;
        while(current != target)
        {
            current += change;
            RubiesAmount.text = "" + current;
            yield return new WaitForSecondsRealtime(0.1f);
        }
    }*/

    private IEnumerator DelayButtonSelect(Button Button)
    {
        yield return null;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(Button.gameObject);
    }

}
