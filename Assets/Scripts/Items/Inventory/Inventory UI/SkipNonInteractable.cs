﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SkipNonInteractable : MonoBehaviour, ISelectHandler
{
    private Selectable Selectable;

    private void Awake()
    {
        Selectable = GetComponent<Selectable>();
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (Selectable.interactable)
        {
            return;
        }

        if(Input.GetAxis("Horizontal") < 0)
        {
            Selectable nextSelectable = Selectable.FindSelectableOnLeft();
            StartCoroutine(DelaySelect(nextSelectable));
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            Selectable nextSelectable = Selectable.FindSelectableOnRight();
            StartCoroutine(DelaySelect(nextSelectable));
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            Selectable nextSelectable = Selectable.FindSelectableOnDown();
            StartCoroutine(DelaySelect(nextSelectable));
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            Selectable nextSelectable = Selectable.FindSelectableOnUp();
            StartCoroutine(DelaySelect(nextSelectable));
        }
    }

    private IEnumerator DelaySelect(Selectable nextSelectable)
    {
        yield return new WaitForEndOfFrame();

        if (nextSelectable != null || !nextSelectable.gameObject.activeInHierarchy)
            nextSelectable.Select();
        else
        {
            Debug.LogError("Explicit selectable navigation isn't configured correctly");
        }
    }
}
