﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EquipButton : MonoBehaviour, ISelectHandler, IDeselectHandler {

    private Transform obj;
    public int Index;

    public void Start()
    {
        obj = transform.GetChild(0);
        obj.gameObject.SetActive(false);
    }

    public void Equip()
    {
        EquipListener.Equip(Index);
    }

    public void OnSelect(BaseEventData eventData)
    {
        obj.gameObject.SetActive(true);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        obj.gameObject.SetActive(false);
    }
}
