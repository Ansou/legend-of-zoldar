﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PickUpItem : MonoBehaviour {

    public enum ItemType {HeartContainer, Heart, Bomb, Stopwatch, Fairy, RupieOne, RupieFive, Key, MagicalShield, Bow, Candle}
    public ItemType item;

    public GameObject player;

	void Start () {

	}
	
	void Update () {
		
	}

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            PickUp(item);
            Destroy(gameObject);
        }
    }

    public void PickUp(ItemType type)
    {
        player = PlayerController.Instance.gameObject;

        if (type == ItemType.HeartContainer)
        {

            player.GetComponent<Health>().IncreaseMaxHealth();
            player.GetComponent<Health>().Heal(2);
            
        }
        else if (type == ItemType.Heart)
        {
            player.GetComponent<Health>().Heal(2);
        }
        else if (type == ItemType.Bomb)
        {
            player.GetComponent<Inventory>().AddBomb();
        }
        else if (type == ItemType.Stopwatch)
        {
            //Stoppa alla enemy movements
        }
        else if (type == ItemType.Fairy)
        {
            int allHearts = player.GetComponent<Health>().MaxHealth;
            player.GetComponent<Health>().Heal(allHearts);
        }
        else if (type == ItemType.RupieOne)
        {
            player.GetComponent<Inventory>().AddRupies(1);
        }
        else if(type == ItemType.RupieFive)
        {
            player.GetComponent<Inventory>().AddRupies(5);
        }
        else if (type == ItemType.Key)
        {
            player.GetComponent<Inventory>().AddKey();
        }

        return;
    }

    private void DestroyObject()
    {
        Destroy(gameObject);
    }
}
