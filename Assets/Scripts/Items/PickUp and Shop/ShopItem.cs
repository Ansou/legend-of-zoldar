﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem : PickUpItem {

    public GameObject shopKeeper;
    public int cost;
    public bool oneTimeItem;
    public int maxValue;

	void Start () {
		
	}
	
	void Update () {
		
	}

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            int currentRupies = player.GetComponent<Inventory>().GetRupies();
            if (currentRupies >= cost)
            {
                PickUp(item);
                shopKeeper.GetComponent<Shop>().hideItems();
            }

        }
    }

    public void PickUp(ItemType type)
    {
        if (type == ItemType.Heart)
        {
            int currentHearts = player.GetComponent<Health>().CurrentHealth;
            if (currentHearts < player.GetComponent<Health>().MaxHealth)
            {
                player.GetComponent<Inventory>().PayRupies(cost);
                player.GetComponent<Health>().Heal(2);
            }
        }
        else if (type == ItemType.Bomb)
        {
            if (!player.GetComponent<Inventory>().FullOnBombs())
            {
                player.GetComponent<Inventory>().AddBomb();
            }
            player.GetComponent<Inventory>().AddBomb();
        }
        else if (type == ItemType.MagicalShield)
        {

        }
        else if (type == ItemType.Candle)
        {
            //if (!Inventory.Instance.HasItem(LightItem))
            //{
             //   player.GetComponent<Inventory>().SetItemActive(LightItem);
            //}
        }

        return;
    }
}
