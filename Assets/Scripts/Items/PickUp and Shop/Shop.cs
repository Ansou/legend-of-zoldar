﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {

    public List<GameObject> shopItems = new List<GameObject>();

	void Start () {

	}
	
	void Update () {
		
	}

    public void hideItems()
    {
        for (int i = 0; i < shopItems.Count; i++)
        {
            shopItems[i].SetActive(false);
        }
    }
}
