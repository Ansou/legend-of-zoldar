﻿using UnityEngine;

public class Health : MonoBehaviour
{
    public int MaxHealth = 6;
    public float InvincibilityTime = 1f;

    [SerializeField] public int CurrentHealth; //{ get; private set; }


    private Timer invincibilityTimer_;


    public delegate void DeathEvent();

    public delegate void DamageEvent(int damage, float knockbackTime, Vector2 knockbackVelocity);

    public delegate void HealthChangedEvent(Health health);

    public DeathEvent OnDeath = delegate { };
    public DamageEvent OnDamaged = delegate { };
    public HealthChangedEvent OnHealthChanged = delegate { };
    public HealthChangedEvent OnMaxHealthChanged = delegate { };


    private void Start()
    {
        CurrentHealth = MaxHealth;
        invincibilityTimer_ = new Timer(InvincibilityTime, true);
    }


    // Update is called once per frame
    private void Update()
    {
        var deltaTime = Time.deltaTime;
        invincibilityTimer_.Update(deltaTime);
    }


    public void Damage(int damage, float knockbackTime, Vector2 knockbackVelocity)
    {
        if (!invincibilityTimer_.IsDone) return;

        invincibilityTimer_.Reset();

        CurrentHealth -= damage;
        OnDamaged(damage, knockbackTime, knockbackVelocity);

        if (CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            if(tag == "Enemy")
            {
                SuperAudioManager.AudioInstance.Die("EnemyDie");
            }

            if(name == "Player")
            {
                SuperAudioManager.AudioInstance.Die("PlayerDie");
            }
            OnDeath();
        }

        if(name == "Player")
            SuperAudioManager.AudioInstance.PlayerAction("PlayerHit");

        if (tag == "Enemy")
            SuperAudioManager.AudioInstance.PlayerAction("EnemyHit");


        OnHealthChanged(this);
    }


    public void Heal(int healAmount)
    {
        CurrentHealth += healAmount;
        if (CurrentHealth > MaxHealth) CurrentHealth = MaxHealth;

        OnHealthChanged(this);
    }


    public void IncreaseMaxHealth()
    {
        MaxHealth += 2;
        OnMaxHealthChanged(this);
    }
}