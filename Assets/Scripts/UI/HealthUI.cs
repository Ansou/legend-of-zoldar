﻿using UnityEngine;


public class HealthUI : MonoBehaviour
{
    public Transform[] HalfHearts;
    public Transform[] HeartContainers;


    // Use this for initialization
    public void SetUp()
    {
        var playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        playerHealth.OnHealthChanged += OnHealthChanged;
        playerHealth.OnMaxHealthChanged += OnMaxHealthChanged;
        OnHealthChanged(playerHealth);
        OnMaxHealthChanged(playerHealth);
    }


    private void OnHealthChanged(Health health)
    {
        for (var i = 0; i < HalfHearts.Length; ++i)
        {
            HalfHearts[i].gameObject.SetActive(i < health.CurrentHealth);
        }
    }


    private void OnMaxHealthChanged(Health health)
    {
        for (var i = 0; i < HeartContainers.Length; ++i)
        {
            HeartContainers[i].gameObject.SetActive(i < health.MaxHealth / 2);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.H)){
            PlayerController.Instance.GetComponent<Health>().Damage(1,0, Vector2.zero);
        }
    }
}