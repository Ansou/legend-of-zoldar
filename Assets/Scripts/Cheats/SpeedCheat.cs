﻿using UnityEngine;


[RequireComponent(typeof(Controller2D))]
public class SpeedCheat : MonoBehaviour
{
    public float Speed = 15f;
    public string Key = "l";


    private float regularSpeed_;
    private Controller2D controller_;
    private bool on_;


    // Use this for initialization
    private void Start()
    {
        controller_ = GetComponent<Controller2D>();
        regularSpeed_ = controller_.Velocity.MaxSpeed;
    }


    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(Key))
        {
            on_ = !on_;
            controller_.Velocity.MaxSpeed = on_ ? Speed : regularSpeed_;
        }
    }
}