﻿using UnityEngine;

public class BlinkForwardCheat : MonoBehaviour
{
    public float BlinkDistance = 1f;
    public string Key = "p";


    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(Key))
        {
            transform.position += transform.up * BlinkDistance;
        }
    }
}