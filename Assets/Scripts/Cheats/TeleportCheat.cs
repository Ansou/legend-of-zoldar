﻿using UnityEngine;

public class TeleportCheat : MonoBehaviour
{
    public string Key = "0";


    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(Key))
        {
            var player = GameObject.FindGameObjectWithTag("Player");
            if (player)
            {
                player.transform.position = transform.position;
            }
        }
    }
}