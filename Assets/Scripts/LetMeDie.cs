﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetMeDie : MonoBehaviour {

	private void OnTriggerEnter2D(Collider2D otehr) {
		if (otehr.gameObject.CompareTag ("Player")) {
			otehr.GetComponent<Health> ().Damage (1000, 0.0f, Vector2.zero);
		}
	}
}
