﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonEntrance : Entrance
{
    public bool ToDungeon;
    public Transform ReturnPoint;

    protected override void Perform()
    {
        GameManager.Instance.LoadScenes(ToDungeon, true);
        Debug.Log("Going");
    }
}
