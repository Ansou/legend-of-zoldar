﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Item
{
    public GameObject Arrow;

    public override void Use()
    {
        //if inventory consume arrow
        Instantiate(Arrow, PlayerController.Instance.transform.position, Quaternion.identity);
    }
}
