﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDoor : MonoBehaviour
{
    private bool locked;
    public bool lockOnStart;

    void Start()
    {
        SetUp();
    }

    public void SetUp()
    {
        ChangeDoorState(lockOnStart);
        locked = lockOnStart;
    }

    public void ChangeDoorState(bool newLockedState)
    {
        locked = newLockedState;
        GetComponent<Renderer>().enabled = locked;
        GetComponent<Collider2D>().enabled = locked;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && locked)
        {
            //if inventory.consumeKey
            ChangeDoorState(false);
        }
    }
}
