﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SuperAudioManager : MonoBehaviour {


    public static SuperAudioManager AudioInstance = null;

    private AudioManager audioManager;

    public string stairScene, secretRoomScene;

    public List<GameObject> enemies = new List<GameObject>();

    private Room currentRoom;



    void Awake()
    {
        if(AudioInstance == null)
        {
            AudioInstance = this;
        }
        else if(AudioInstance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


	// Use this for initialization
	void Start () {
        audioManager = GetComponent<AudioManager>();
        RoomManager.OnRoomTransition.AddListener(ChangingScene);
        Invoke("OverWorldInit", 0.2f);
        StartCoroutine(RunUpdate());
    }
	
	// Update is called once per frame
	void Update () {

        foreach(GameObject go in enemies)
        {
            if(go != null)
            {

            }
            else
            {
                Die("Enemy");
                break;
            }
        }
	}

    private IEnumerator RunUpdate()
    {

        for (;;)
        {
            UpdateList();

                        yield return new WaitForSeconds(0.4f);
        }       
    }

    //Call when Changing scenes.
    private void ChangingScene()
    {

        enemies.Clear();           

        string sceneName = SceneManager.GetActiveScene().name;

        if(sceneName == stairScene)
        {
            audioManager.PlayPrio("Stairs");
        }

        if(sceneName == secretRoomScene)
        {
            audioManager.PlaySound("SecretRoom");
        }

        currentRoom = GameObject.FindObjectOfType<Room>();
        //currentRoom.enemiesSpawned.AddListener(PopulateList);
        Invoke("PopulateList", 1f);
    }

    //Adds all enemies in scene to list to later play deathSound.
    private void PopulateList()
    {
        UpdateList();
    }

    private void CheckIntegrity()
    {
        int index = 0;            
        foreach(GameObject go in enemies)
        {            
            if(go == null)
                enemies.RemoveAt(index);
            
            index++;
        }
    }

    private void UpdateList()
    {

        List<GameObject> goList = new List<GameObject>();
        GameObject[] arr = GameObject.FindGameObjectsWithTag("Enemy");

        for(int i = 0; i < arr.Length; i++)
        {
            if (enemies.Contains(arr[i]))
            {
                //DO nothing
            }else
            {
                enemies.Add(arr[i]);
            }        
        }
    }

    //If anyone with a healthscript takes damage, this should be called.
    public void TakeDamage(string type)
    {
        switch (type)
        {
            case "Player":
                audioManager.PlaySound("PlayerHit");
                break;
            case "Enemy":
                audioManager.PlaySound("EnemyHit");
                break;
        }     
    }

    //If anyone dies this is called. Player calls from his script, enemies call from the local enemies list.
    public void Die(string type)
    {
        switch (type)
        {
            case "Player":
                audioManager.PlaySound("PlayerDie");

                break;
            case "Enemy":
                audioManager.PlaySound("EnemyDie");
                break;
        }
    }

    public void GetRupy(int amount)
    {
        float _amount = amount;

        audioManager.PlaySound("Rupy", _amount);
    }

    //When player does actions like swinging sword, this should be called.
    public void PlayerAction(string type)
    {
        audioManager.PlaySound(type);
    }

    //Starts playing overworld from start.
    public void OverWorldInit()
    {
        audioManager.PlaySound("OverWorldStart");
        Invoke("OverWorldLoop", 6.8f);
    }

    //Loops the correct loop-part of this song.
    public void OverWorldLoop()
    {
        audioManager.PlaySound("OverWorldLoop");
    }



}
