﻿using UnityEngine;
using System;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    private Sound[] tempSounds;

    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();

            s.source.clip = s.clip;
            s.source.volume = s.settings.volume;
            s.source.pitch = s.settings.pitch;
            s.source.loop = s.settings.loop;
        }
    }

    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            if (!s.source.isPlaying)           
                s.source.Play();          
            else if (s.settings.forcePlay)
                s.source.Play();
        }
        else
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
    }

    public void PlaySound(string name, float time)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            StartCoroutine(LoopTimer(time, name));

        }
        else
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
    }

    public void StopSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            s.source.Stop();
        }
        else
        {
            Debug.LogWarning("Sound: " + name + " not found.");
            return;
        }
    }

    public void PlayPrio(string name)
    {
        foreach(Sound playingSounds in sounds)
        {
            playingSounds.source.Stop();
        }

        Sound s = Array.Find(sounds, sound => sound.name == name);

        if(s != null)
            s.source.Play();     
    }

    public IEnumerator LoopTimer(float timer, string sound_name)
    {
        PlaySound(sound_name);
        yield return new WaitForSeconds(timer);
        StopSound(sound_name);
    }
}
