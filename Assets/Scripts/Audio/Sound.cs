﻿using UnityEngine;

[System.Serializable]
public class Sound {

    public string name;

    public AudioClip clip;
    public AudioSettings settings;
    [HideInInspector]
    public AudioSource source;

}
