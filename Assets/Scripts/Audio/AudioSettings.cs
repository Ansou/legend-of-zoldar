﻿using UnityEngine;

[System.Serializable]
public class AudioSettings  {


    [Range(0f, 1f)]
    public float volume = 1;

    [Range(0.1f, 3f)]
    public float pitch = 1;

    [Tooltip("Call AudioPlayer's StopSound() to stop looping.")]
    public bool loop;

    [Tooltip("Always plays sound even if it's already playing.")]
    public bool forcePlay;

}
