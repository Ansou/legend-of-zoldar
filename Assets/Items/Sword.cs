﻿using UnityEngine;

public class Sword : MonoBehaviour
{
    public int Damage;
    public GameObject projectile;
    public float BeamSpeed = 17f;

    public void Upgrade()
    {
        Damage *= 2;
    }

    public void Attack(Vector2 direction, bool Beam)
    {

        SuperAudioManager.AudioInstance.PlayerAction("Slash");

        var origin = direction.normalized;
        origin.x *= 1.5f;
        origin.y *= 1.5f;
        var hit = Physics2D.OverlapBoxAll(new Vector2(transform.position.x, transform.position.y) + origin,
            new Vector2(1, 1), Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg);
        foreach (var other in hit)
        {
            if (other.CompareTag("Enemy"))
            {

                SuperAudioManager.AudioInstance.PlayerAction("EnemyHit");


                var health = other.GetComponent<Health>();
                if (health)
                {
                    health.Damage(Damage, 0f, Vector2.zero);
                }
            }
        }

        if (Beam)
        {            
            Swordbeam(direction, origin);
            SuperAudioManager.AudioInstance.PlayerAction("Beam");
        }

        
    }

    public void Swordbeam(Vector2 direction, Vector2 origin)
    {
        var proj = Instantiate(projectile, new Vector2(transform.position.x, transform.position.y) + origin, transform.rotation);
        proj.GetComponent<Projectile>().OnSpawn(direction, Damage, BeamSpeed, 0f, 0f);
    }
}