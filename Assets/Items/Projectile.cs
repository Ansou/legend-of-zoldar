﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Projectile : MonoBehaviour
{
    //public LayerMask HitMask_;
	public bool hitPlayer = true;

	private Timer maxTime;
    private int damage;
    private Vector2 direction;
    private float speed;
	private float knockbackTime;
	private float knockbackSpeed;
    
    private int[] deflectDir = { -1, 1 };
    // Update is called once per frame
    public void Update () {
        transform.position = transform.position + new Vector3(direction.x, direction.y, 0f) * speed * Time.deltaTime;
		if (maxTime.Update (Time.deltaTime))
			Destroy (gameObject);
    }

	public void Awake(){
		maxTime = new Timer (20f);

	}

	public void OnSpawn(Vector2 direction, int damage, float speed, float knockTime, float knockSpeed)
    {
        this.damage = damage;
        this.speed = speed;
        this.direction = direction;
		knockbackTime = knockTime;
		knockbackSpeed = knockSpeed;
    }

    public void Deflect()
    {

        direction *= -1;
        var v = new Vector3(direction.x, direction.y, 0);
        v.x += ((Mathf.Abs(v.x)-1 )* deflectDir[Random.Range(0, deflectDir.Length)]) * Random.Range(0.1f, 0.4f);
        v.y += ((Mathf.Abs(v.y)-1) * deflectDir[Random.Range(0, deflectDir.Length)]) * Random.Range(0.1f, 0.4f);
        direction = v;
        speed /= 4;
        SuperAudioManager.AudioInstance.PlayerAction("Shield");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
		
		if (collision.CompareTag ("Player") && hitPlayer) {
			if (collision.GetComponent<Shield> ().Active &&
			     Vector2.Dot (collision.transform.up, direction.normalized) < -0.8f) {
				Deflect ();
				return;
			}


				collision.GetComponent<Health> ().Damage (damage, knockbackTime, direction.normalized * knockbackSpeed);
                

            Destroy (gameObject);

		} else if (collision.CompareTag ("Enemy") && !hitPlayer) {
			collision.GetComponent<Health> ().Damage (damage, knockbackTime, direction.normalized * knockbackSpeed);
			Destroy(gameObject);
		} else if(!collision.CompareTag ("Enemy") && !collision.CompareTag ("Player")) {
			Destroy(gameObject);
		}
		
        
        //if ((collision.gameObject.layer & HitMask_) == 0) return;
		//var health = collision.gameObject.GetComponent<Health>();
		
        //var health = collision.GetComponent<Health>();
        //if (health)
        //{
        //    // damage
        //}

        
    }
}
