﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : ScriptableObject
{

    public Sprite Icon;

    public virtual void Initilize() { }

    public abstract void Use();

}
