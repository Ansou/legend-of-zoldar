﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Items/Light")]

public class LightItem : Item
{

    public GameObject LightPrefab;
    public float Offset;

    public bool CanUse = true;

    public override void Initilize()
    {
        RoomManager.OnRoomTransition.AddListener(() => CanUse = true);
        CanUse = true;
    }

    public override void Use()
    {
        if (!CanUse) return;
        Instantiate(LightPrefab, PlayerController.Instance.transform.position + PlayerController.Instance.transform.up * Offset, Quaternion.identity);
        CanUse = false;
    }
}
