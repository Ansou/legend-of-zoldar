﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Items/Bomb")]
public class Bomb : Item
{
    public GameObject BombPrefab;
    public float Offset;

    public override void Use()
    {
        if (Inventory.Instance.ConsumeBomb())
        {
            Instantiate(BombPrefab, PlayerController.Instance.transform.position + PlayerController.Instance.transform.up * Offset, Quaternion.identity);
        }
    }
}