﻿using UnityEngine;

public class Boomerang : Item
{
    public GameObject BoomerangPrefab;
    public float Offset;

    public override void Use()
    {
        Instantiate(BoomerangPrefab,
            PlayerController.Instance.transform.position + PlayerController.Instance.transform.up * Offset,
            PlayerController.Instance.transform.rotation);
    }
}