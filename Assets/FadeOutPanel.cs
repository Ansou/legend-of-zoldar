﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOutPanel : MonoBehaviour
{
    private static FadeOutPanel _instance;

    public Image Panel;
    public float FadeToBlackDuration, FadeBackTime;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public static IEnumerator Fade(float targetAlpha)
    {
        float duration = targetAlpha < 0.5 ? _instance.FadeBackTime : _instance.FadeToBlackDuration;
        float t = 0.0f;
        Color startColor = _instance.Panel.color; startColor.a = 1.0f - targetAlpha;
        Color endColor = startColor; endColor.a = targetAlpha;

        while (t < duration)
        {
            t += Time.deltaTime;
            _instance.Panel.color = Color.Lerp(startColor, endColor, t / duration);
            yield return null;
        }
    }

    public static void SetAlpha(float alpha)
    {
        Color color = _instance.Panel.color;
        color.a = alpha;
        _instance.Panel.color = color;
    }
}
