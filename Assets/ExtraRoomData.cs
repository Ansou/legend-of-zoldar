﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ExtraRoomData")]
public class ExtraRoomData : ScriptableObject
{
    public bool SwordRoomCleared;
}
