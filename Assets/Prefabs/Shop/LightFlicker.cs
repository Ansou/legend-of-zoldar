﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    public float Scale;
    public float Max, Min;

    private Light _light;

    private void Start()
    {
        _light = GetComponent<Light>();
    }

    private void Update()
    {
        _light.intensity = Mathf.Lerp(Min, Max, Mathf.PerlinNoise(Time.time * Scale, 0f));
    }
}
