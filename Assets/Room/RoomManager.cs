﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Events;

public class RoomManager : MonoBehaviour
{
    private static RoomManager _instance;
	public static Room ActiveRoom;
	public static Room DefaultRoom;

	public static UnityEvent OnRoomTransition = new UnityEvent();

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public static void SetUpRooms(bool setActiveRoom = true)
    {
        Room[] rooms = FindObjectsOfType<Room>();
        foreach (Room room in rooms)
        {
			if (room.Default) DefaultRoom = room;
		}
    }

	public static void SetDefaultRoomActive() {
		SetActiveRoom (DefaultRoom, true);
	}

    public static void SetActiveRoom(Room newRoom, bool forceMovement)
    {
		OnRoomTransition.Invoke ();
        _instance.StartCoroutine(_instance.SetActiveRoomCoroutine(newRoom, forceMovement));
    }
    private IEnumerator SetActiveRoomCoroutine(Room newRoom, bool forceMovement)
    {
        Room oldRoom = ActiveRoom;
		if (oldRoom != null) {
			oldRoom.Deactivate ();
		}
        newRoom.SetCollidersActive(false);
        ActiveRoom = newRoom;
        
        if (forceMovement)
            CameraMovement.Instance.transform.position = newRoom.transform.position;
        else
            yield return CameraMovement.Instance.MoveToNewRoom(newRoom);
		
		newRoom.Activate ();

        if (oldRoom == null || oldRoom == newRoom) yield break;
        oldRoom.SetCollidersActive(true);

        yield return null;
    }
}
