﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraRoom : MonoBehaviour
{
    public Transform PlayerSpawnPoint;

    public Vector2 LocalCameraPosition;
    public Vector3 PlayerSpawnPosition { get { return PlayerSpawnPoint.position; } }
    public Vector3 CameraPosition { get { return transform.position + (Vector3)LocalCameraPosition; } }
}
