﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCollider : MonoBehaviour
{
    private Room _parentRoom;

    private void Awake()
    {
        _parentRoom = GetComponentInParent<Room>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            RoomManager.SetActiveRoom(_parentRoom, false);
        }
    }
}
