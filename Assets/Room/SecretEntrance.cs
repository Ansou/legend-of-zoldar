﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretEntrance : Entrance
{

    public string ExtraRoomId;
    public Transform ReturnPoint;

    protected override void Perform()
    {
        GameManager.Instance.LoadExtraRoom(ExtraRoomId, ReturnPoint, GetComponentInParent<Room>());
    }
}
