﻿using System.Collections;
using System.Collections.Generic;
using Rewired.ControllerExtensions;
using UnityEngine;

public class WorldBomb : MonoBehaviour
{
    public float Duration;
    public CircleCollider2D ExplosionCollider;
    private float _timer;
    public LayerMask EnemyLayerMask;
    public int Damage;
    public ParticleSystem ExplosionParticles;
    public float KnockbackTime;
    public float KnockbackForce;

    [Range(0.0f, 1.0f)] public float CameraShakeAmount = 0.3f;

	private void Update ()
	{
	    _timer += Time.deltaTime;
	    if (_timer > Duration)
	    {
	        Explode();
	    }
	}

    private void Explode()
    {
        Collider2D[] colls = Physics2D.OverlapCircleAll(transform.position, ExplosionCollider.radius, EnemyLayerMask);
        foreach (Collider2D coll in colls)
        {
            DestructibleTerrain terrain = coll.GetComponent<DestructibleTerrain>();
            if (terrain != null && terrain.TerrainType == DestructibleTerrainType.Rock) terrain.Destroy();

            Health health = coll.GetComponent<Health>();
            if(health == null) continue;
            Vector3 dirToHit = (transform.position - coll.transform.position).normalized;
            health.Damage(Damage, KnockbackTime, dirToHit * KnockbackForce);
        }

        CameraEffects.AddIntensity(CameraShakeAmount);
        GameObject particles = Instantiate(ExplosionParticles, transform.position, Quaternion.identity).gameObject;
        Destroy(particles, 3f);
        Destroy(gameObject);
    }
}
