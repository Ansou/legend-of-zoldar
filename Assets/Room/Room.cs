﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Room : MonoBehaviour
{
    public bool Default;
    private RoomCollider[] _colliders;
    private Controller2D[] _enemies;
	public LayerMask NoSpawnLayers;

    [Header("Events")]
    public UnityEvent RoomEntered;
    public UnityEvent RoomCleared;
    public UnityEvent RoomReset;
    private bool _cleared;

    private void Awake()
    {
		_colliders = GetComponentsInChildren<RoomCollider> ();
    }
    private void Start() {
		_enemies = GetComponentsInChildren<Controller2D> ();
        DespawnEnemies();
		foreach (Controller2D e in _enemies)
		{
            
			//e.gameObject.GetComponentInChildren<Renderer> ().enabled = false;
		}
	}

    public void Activate()
    {
        RoomEntered.Invoke();
		SpawnEnemies ();
    }
    public void Deactivate()
    {
		DespawnEnemies ();
    }
    public void SetCollidersActive(bool active)
    {
        foreach (RoomCollider coll in _colliders) coll.gameObject.SetActive(active);
    }

    //Enemies
	private void SpawnEnemies() {
        if(_enemies == null) return;
		foreach(Controller2D e in _enemies) {
		    Spawn (e);
		}
	}


	private void Spawn(Controller2D newEnemy) {
		//puffeffekt 
		Vector3 spawnPos = transform.position;
		spawnPos.x += Random.Range (-8, 9);
		spawnPos.y += Random.Range (-5, 5);

		RaycastHit2D hit = Physics2D.BoxCast (spawnPos + Vector3.back, Vector2.one, 0.0f, Vector3.forward, 1.1f, NoSpawnLayers);
		if (hit.collider != null)
		{
			Spawn (newEnemy);
		}
		else 
		{
            // HACK: This if check is obviously ridiculous
            if(!(newEnemy is Boss1Controller))
			    newEnemy.gameObject.transform.position = spawnPos;
            newEnemy.gameObject.SetActive(true);
		}
    }
	private void DespawnEnemies() {
		foreach(Controller2D e in _enemies) {
			//om enemy itneär död
			DeSpawn (e);
		}
	}
	private void DeSpawn(Controller2D enemy) {
	    enemy.gameObject.SetActive(false);
	}
}
