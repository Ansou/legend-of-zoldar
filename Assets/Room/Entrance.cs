﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entrance : MonoBehaviour
{

    public float MaxDistanceToCenter = 0.1f;
	public float playerStunTime = 2.0f;
    private bool _performed;

    private void OnTriggerStay2D(Collider2D other)
    {
        if(_performed || !other.CompareTag("Player")) return;

        if (Vector2.Distance(other.transform.position, transform.position) < MaxDistanceToCenter)
        {
            _performed = true;
            other.transform.position = transform.position;
			other.GetComponent<PlayerController> ().Stun (playerStunTime, Vector2.zero);
            //Lock player
            Perform();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _performed = false;
    }

    protected abstract void Perform();
}
