﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnDoor : Entrance
{

    protected override void Perform()
    {
        GameManager.Instance.ReturnTo();
    }
}
