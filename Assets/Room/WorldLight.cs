﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldLight : MonoBehaviour {

    public float LifeDuration;
    public int Damage;
    public float Speed;
    public CircleCollider2D Collider;
    public LayerMask HitLayers;
    public float KnockbackTime;
    public float KnockBackForce;

    private Vector2 _velocity;
    private float _timer;
    private List<Collider2D> _hitColliders = new List<Collider2D>();

    private void Start()
    {
        _velocity = PlayerController.Instance.transform.up * Speed * Time.deltaTime;
    }

    private void Update ()
    {
        Burn();

        transform.Translate(_velocity);
        _timer += Time.deltaTime;
        if (_timer > LifeDuration) End();
    }

    private void Burn()
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, Collider.radius, transform.up, _velocity.magnitude, HitLayers);

        foreach (RaycastHit2D hit in hits)
        {
            if(hit.collider == null || _hitColliders.Contains(hit.collider)) return;
            _hitColliders.Add(hit.collider);

            DestructibleTerrain destructible = hit.collider.GetComponent<DestructibleTerrain>();
            if (destructible != null && destructible.TerrainType == DestructibleTerrainType.Bush) destructible.Destroy();

            Health health = hit.collider.GetComponent<Health>();
            if (health != null) health.Damage(Damage, KnockbackTime, _velocity.normalized * KnockBackForce); //TODO: FIX THIS

            
        }
    }

    private void End()
    {
        Destroy(gameObject);        
    }
}
